﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float hp = 15f;
    public int wood = 0;
    public float energy = 15f;

    public GameObject playerPanel;
    public GameObject damagePanel;
    public GameObject gameOverPanel;
    public GameObject cloudPanel;

    private Slider healthSlider;
    private Slider energySlider;
    private Text woodValueText;

    private float lastHp;

    void Start()
    {
        healthSlider    = playerPanel.transform.Find("HealthSlider").gameObject.GetComponent<Slider>();
        energySlider    = playerPanel.transform.Find("EnergySlider").gameObject.GetComponent<Slider>();
        woodValueText   = playerPanel.transform.Find("WoodLabel").Find("WoodValue").gameObject.GetComponent<Text>();

        lastHp = hp;
        StartCoroutine(PlayerRoutine());
    }

    void Update()
    {
        if (hp < lastHp)
        {
            lastHp = hp;
            StartCoroutine(damage());

            if (hp <= 0)
            {
                StartCoroutine(gameOver());
            }
        }

        healthSlider.value = 1f / 20f * hp;
        energySlider.value = 1f / 20f * energy;
        woodValueText.text = wood.ToString();

        if (Input.anyKey)
        {
            Destroy(cloudPanel);
        }
    }

    private IEnumerator PlayerRoutine()
    {
        while (true)
        {
            if (energy > 0)
            {
                energy--;
            }
            else
            {
                hp--;
            }

            yield return new WaitForSeconds(4);
        }
    }

    private IEnumerator damage()
    {
        damagePanel.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        damagePanel.SetActive(false);
    }

    private IEnumerator gameOver()
    {
        gameOverPanel.SetActive(true);

        yield return new WaitForSeconds(5);

        Application.LoadLevel(index: Application.loadedLevel);
    }

    public IEnumerator youWin()
    {
        gameOverPanel.transform.Find("Text").gameObject.GetComponent<Text>().text = "You win";

        gameOverPanel.SetActive(true);

        yield return new WaitForSeconds(10);

        Application.LoadLevel(index: Application.loadedLevel);
    }
}