﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    [SerializeField]
    private AudioClip axeHit;

    [SerializeField]
    private AudioClip axeMiss;

    [SerializeField]
    private AudioClip axePick;

    [SerializeField]
    private AudioClip axeHurtAnimal;

    //Variables
    public GameObject axe;
    private Animator _axeAnimator;
    private AudioSource axeAudioSource;
    private bool axeIsEquiped = true;

    private GameObject boatPanel;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").transform.GetComponent<GameManager>();

        _axeAnimator = axe.GetComponent<Animator>();
        axeAudioSource = axe.GetComponent<AudioSource>();

        boatPanel = GameObject.FindGameObjectWithTag("BoatPanel");
        boatPanel.SetActive(false);
    }

    private void Update()
    {
        if (!axe.activeSelf && Input.GetKeyDown(KeyCode.Alpha1))
        {
            axeIsEquiped = true;
            axe.SetActive(true);

            axeAudioSource.clip = axePick;
            axeAudioSource.Play();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            axeIsEquiped = false;
            axe.SetActive(false);
        }

        //Raycast
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;

        //Origin, Direction, RaycastHit, Length
        if (Physics.Raycast(transform.position, fwd, out hit, 10))
        {

            if (!boatPanel.activeSelf && hit.collider.tag == "Boat")
            {
                boatPanel.SetActive(true);
            }
            else if (boatPanel.activeSelf && hit.collider.tag != "Boat")
            {
                boatPanel.SetActive(false);
            }

            if (Input.GetMouseButtonDown(0) && axeIsEquiped == true)
            {
                axeAudioSource.clip = axeMiss;

                if (hit.collider.tag == "Tree")
                {
                    Tree treeScript = hit.collider.gameObject.GetComponent<Tree>();
                    treeScript.treeHealth--;
                    axeAudioSource.clip = axeHit;
                }
                else if (hit.collider.tag == "Tiger")
                {
                    Tiger tigerScript = hit.collider.gameObject.GetComponent<Tiger>();
                    tigerScript.tigerHealth--;
                    axeAudioSource.clip = axeHurtAnimal;
                }
                else if (hit.collider.tag == "Boat")
                {
                    int woodForHit = 3;

                    if (gameManager.wood >= woodForHit)
                    {
                        Boat boatScript = hit.collider.gameObject.GetComponent<Boat>();
                        boatScript.currentBoatStatus++;
                        axeAudioSource.clip = axeHurtAnimal;
                        gameManager.wood = gameManager.wood - woodForHit;
                    }
                }

                _axeAnimator.SetTrigger("isChopping");

                axeAudioSource.Play();
            }
        }
    }
}