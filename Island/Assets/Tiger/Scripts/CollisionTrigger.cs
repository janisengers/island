﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTrigger : MonoBehaviour {

    private Tiger parent;

    void Start()
    {
        parent = transform.parent.GetComponent<Tiger>();
    }

    void OnTriggerEnter(Collider other)
    {
        parent.OnChildTriggerEnter(gameObject, other);
    }

    void OnTriggerExit(Collider other)
    {
            parent.OnChildTriggerExit(gameObject, other);
    }
}
