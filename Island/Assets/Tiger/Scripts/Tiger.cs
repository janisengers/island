﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiger : MonoBehaviour {
    private enum State
    {
        Idle,
        Walking,
        Attacking,
        Dead
    }

    //Variables
    GameObject thisTiger;
    public int tigerHealth = 5;

    [SerializeField]
    private State currentState = State.Idle;
    private State lastState;

    [SerializeField]
    private float walkingSpeed = 1f;
    [SerializeField]
    private float runingSpeed = 3f;

    [SerializeField]
    List<AudioClip> tigerAttack;
    [SerializeField]
    AudioClip tigerRoar;

    private bool stateMachineInitiated  = false;
    private bool canDamage              = false;

    private AudioSource _audioSource;
    private Animator _animator;
    private Transform _playerTransform;

    private Coroutine StateChangerCoroutine;

    private int terrainWidth; // terrain size (x)
    private int terrainLength; // terrain size (z)
    private int terrainPosX; // terrain position x
    private int terrainPosZ; // terrain position z

    private Terrain terrain;

    private void Start()
    {
        thisTiger           = gameObject;
        _playerTransform    = GameObject.FindGameObjectWithTag("Player").transform;
        _animator           = GetComponent<Animator>();
        _audioSource        = GetComponent<AudioSource>();

        StartCoroutine(StateMachine());

        terrain = Terrain.activeTerrain;

        // terrain size x
        terrainWidth = (int)terrain.terrainData.size.x;
        // terrain size z
        terrainLength = (int)terrain.terrainData.size.z;
        // terrain x position
        terrainPosX = (int)terrain.transform.position.x;
        // terrain z position
        terrainPosZ = (int)terrain.transform.position.z;
    }

    private IEnumerator StatesChanger()
    {
        switch (currentState)
        {
            case State.Idle:
                yield return new WaitForSeconds(15);
                currentState = State.Walking;
                break;
            case State.Walking:
                yield return new WaitForSeconds(5);
                currentState = State.Idle;
                break;
        }

        yield return null;
    }

    private IEnumerator StateMachine()
    {
        while (true)
        {
            if (tigerHealth <= 0)
            {
                currentState = State.Dead;
            }

            if (!stateMachineInitiated || lastState != currentState)
            {
                switch (currentState)
                {
                    case State.Idle:
                        InitIdle();
                        StateChangerCoroutine = StartCoroutine(StatesChanger());
                        break;
                    case State.Walking:
                        InitWalking();
                        StateChangerCoroutine = StartCoroutine(StatesChanger());
                        break;
                    case State.Attacking:
                        InitAttacking();
                        break;
                    case State.Dead:
                        InitDead();
                        break;
                }

                stateMachineInitiated = true;
            }

            StateMachineUpdate();

            lastState = currentState;
            yield return null;
        }

    }

    private void InitIdle()
    {
        _animator.SetBool("isWalking", false);
        _animator.SetBool("isRuning", false);
        _animator.SetBool("isAttacking", false);
    }

    private void InitWalking()
    {
        _animator.SetBool("isWalking", true);
        _animator.SetBool("isRuning", false);
        _animator.SetBool("isAttacking", false);
    }

    private void InitAttacking()
    {
        _animator.SetTrigger("isRoaring");
        _animator.SetBool("isWalking", false);
        _animator.SetBool("isAttacking", true);

        StartCoroutine(damagePlayer());
    }

    private void InitDead()
    {
        //Destroy(thisTiger);
        

        int posx = Random.Range(terrainPosX, terrainPosX + terrainWidth);
        int posz = Random.Range(terrainPosZ, terrainPosZ + terrainLength);
        float posy = Terrain.activeTerrain.SampleHeight(new Vector3(posx, 0, posz));

        Vector3 pos = new Vector3(posx, posz, posy);

        thisTiger.transform.position = pos;

        tigerHealth = 5;

        currentState = State.Walking;
    }

    private void StateMachineUpdate()
    {
        switch (currentState)
        {
            case State.Attacking:
                AttackingUpdate();
                break;
            case State.Walking:
                WalkingUpdate();
                break;
        }
    }

    private void WalkingUpdate()
    {
        Vector3 direction = _playerTransform.position - transform.position;
        direction.Normalize();

        Vector3 deltaMovementVector = direction * Time.deltaTime * walkingSpeed;

        transform.position += deltaMovementVector;
        transform.LookAt(_playerTransform);
    }

    private void AttackingUpdate()
    {
        transform.LookAt(_playerTransform);

        if (canDamage)
        {
            _animator.SetBool("isRuning", false);
        }
        else
        {
            _animator.SetBool("isRuning", true);
            Vector3 direction = _playerTransform.position - transform.position;
            direction.Normalize();

            Vector3 deltaMovementVector = direction * Time.deltaTime * runingSpeed;

            transform.position += deltaMovementVector;
        }

        if (!_audioSource.isPlaying)
        {
            _audioSource.clip = tigerAttack[Random.Range(0, tigerAttack.Count)];
            _audioSource.Play();
        }
    }

    private IEnumerator damagePlayer()
    {
        GameManager gameManager = GameObject.FindGameObjectWithTag("GameManager").transform.GetComponent<GameManager>();

        while (true)
        {
            while (!canDamage)
            {
                yield return null;
            }

            gameManager.hp--;

            yield return new WaitForSeconds(1f);
        }
       
    }

    public void OnChildTriggerEnter(GameObject child, Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (child.tag == "CanDamageColider")
            {
                canDamage = true;
            }
            else if (child.tag == "IsInAttackingZoneColider")
            {
                StopCoroutine(StateChangerCoroutine);
                currentState = State.Attacking;
            }
        }
    }

    public void OnChildTriggerExit(GameObject child, Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (child.tag == "CanDamageColider")
            {
                canDamage = false;
            }
        }
    }
}
