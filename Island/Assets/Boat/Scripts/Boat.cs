﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boat : MonoBehaviour
{
    public float boatCompleteStatus   = 20f;
    public float currentBoatStatus   = 0;

    private GameManager gameManager;

    private enum State
    {
        early,
        middle,
        complete
    }

    [SerializeField]
    private State currentState = State.early;
    private State lastState;
    private bool stateMachineInitiated = false;

    private GameObject thisBoat;
    private GameObject boatEarly;
    private GameObject boatMiddle;
    private GameObject boatComplete;
    public Slider progresSlider;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").transform.GetComponent<GameManager>();

        thisBoat        = gameObject;
        boatEarly       = thisBoat.transform.Find("EarlyStage").gameObject;
        boatMiddle      = thisBoat.transform.Find("MiddleStage").gameObject;
        boatComplete    = thisBoat.transform.Find("Complete").gameObject;

        StartCoroutine(StateMachine());
    }

    private IEnumerator StateMachine()
    {
        while (true)
        {
            progresSlider.value = 1 / boatCompleteStatus * currentBoatStatus;

            if (currentBoatStatus > 0.5 * boatCompleteStatus && currentBoatStatus < boatCompleteStatus)
            {
                currentState = State.middle;
            }
            else if (currentBoatStatus >= boatCompleteStatus)
            {
                currentState = State.complete;
            }

            if (!stateMachineInitiated || lastState != currentState)
            {
                switch (currentState)
                {
                    case State.early:
                        boatEarly.SetActive(true);
                        boatMiddle.SetActive(false);
                        boatComplete.SetActive(false);
                        break;
                    case State.middle:
                        boatEarly.SetActive(false);
                        boatMiddle.SetActive(true);
                        boatComplete.SetActive(false);
                        break;
                    case State.complete:
                        boatEarly.SetActive(false);
                        boatMiddle.SetActive(false);
                        boatComplete.SetActive(true);
                        StartCoroutine(gameManager.youWin());
                        break;
                }

                stateMachineInitiated = true;
            }

            lastState = currentState;
            yield return null;
        }
    }
}
