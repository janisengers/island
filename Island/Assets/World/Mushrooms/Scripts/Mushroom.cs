﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameObject.GetComponent<AudioSource>().Play();
            StartCoroutine(destroyMushroom());
        }
    }

    private IEnumerator destroyMushroom()
    {
        yield return new WaitForSeconds(1);

        GameManager gameManager = GameObject.FindGameObjectWithTag("GameManager").transform.GetComponent<GameManager>();

        if (gameManager.hp < 20)
        {
            gameManager.hp++;
        }

        if (gameManager.energy < 20)
        {
            float energy = gameManager.energy + 5;

            if (energy > 20f)
            {
                gameManager.energy = 20f;
            }
            else
            {
                gameManager.energy = energy;
            }
        }
        

        Destroy(gameObject);
    }
}
