﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    //Variables
    GameObject thisTree;
    public int treeHealth = 5;
    private bool isFallen = false;
    private int treeValue;

    private void Start()
    {
        treeValue = treeHealth;
        thisTree = transform.parent.gameObject;
    }

    private void Update()
    {
        if(treeHealth <= 0 && isFallen == false)
        {
            Rigidbody rb = thisTree.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.AddForce(Vector3.forward, ForceMode.Impulse);
            thisTree.GetComponent<AudioSource>().Play();
            StartCoroutine(destroyTree());
            isFallen = true;
        }
    }

    private IEnumerator destroyTree()
    {
        yield return new WaitForSeconds(5);
        GameManager gameManager = GameObject.FindGameObjectWithTag("GameManager").transform.GetComponent<GameManager>();
        gameManager.wood = gameManager.wood + treeValue;
        Destroy(thisTree);
    }
}
